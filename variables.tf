variable "resource_group_name_VM" {
  default     = "myresourcegroupname"
  type        = "string"
  description = "Resource Group name for VM"
}

variable "subnet_ID" {
  default     = "subnetid"
  type        = "string"
  description = "Resource Group subnet for VM"
}


variable "vhd_uri_URL" {
  default     = "vhduri"
  type        = "string"
  description = "Packer image link"
}


variable "subscription_ID" {
  default     = "subscription_id"
  type        = "string"
  description = "Subscription ID"
}

variable "tenant_ID" {
  default     = "tenant_id"
  type        = "string"
  description = "Tenant ID"
}

variable "client_ID" {
  default     = "client_id"
  type        = "string"
  description = "client ID"
}

variable "client_Sec" {
  default     = "client_secret"
  type        = "string"
  description = "client secret"
}


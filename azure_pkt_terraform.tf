
provider "azurerm" {
  version         = "=1.28.0"
  subscription_id = "${var.subscription_ID}"
  tenant_id       = "${var.tenant_ID}"
  client_id       = "${var.client_ID}"
  client_secret   = "${var.client_Sec}"

  skip_provider_registration = "true"

}

resource "azurerm_network_interface" "main" {
  name                = "terraform-PKT-nic"
  location            = "West US"
  resource_group_name = "${var.resource_group_name_VM}"

  ip_configuration {
    name                          = "testconfiguration1"
    subnet_id                     = "${var.subnet_ID}"
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_virtual_machine" "main" {
  name                             = "terraform-PKT-vm"
  location                         = "West US"
  resource_group_name              = "${var.resource_group_name_VM}"
  network_interface_ids            = ["${azurerm_network_interface.main.id}"]
  vm_size                          = "Standard_DS1_v2"
  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true



  storage_os_disk {
    name = "vszOsDisk"
    # source VHD as reference
    image_uri = "https://pipesjenkinsmasterprod01.blob.core.windows.net/system/Microsoft.Compute/Images/packer/pipes-image-rsi-0.2.0-osDisk.7b09089f-7888-40b4-9e05-97ddba44b8ca.vhd"
    # destination VHD to create
    vhd_uri       = "${var.vhd_uri_URL}"
    os_type       = "Linux"
    caching       = "ReadWrite"
    create_option = "FromImage"
  }
  os_profile {
    computer_name  = "hostname"
    admin_username = "testadmin"
    admin_password = "Password1234!"
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
}
